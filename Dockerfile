ARG IMAGE_VERSION=2.7.0
FROM quay.io/fluentd_elasticsearch/fluentd:v${IMAGE_VERSION}

RUN buildDeps="build-essential curl libgeoip-dev ruby-dev  libmaxminddb-dev" \
     && apt-get update \
     && apt-get install \
     -y --no-install-recommends \
     $buildDeps \
     && gem install fluent-plugin-geoip \
     && gem install fluent-plugin-forest \
     && gem install fluent-plugin-rewrite-tag-filter \
     && gem install fluent-plugin-record-modifier \
     && gem install fluent-plugin-record-reformer \
     && gem install fluent-plugin-ua-parser \
     && cd / \
     && curl https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz | gzip -d > GeoLite2-City.mmdb \
     && rm -rf /var/lib/apt/lists/* \
     && gem sources --clear-all \
     && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem
